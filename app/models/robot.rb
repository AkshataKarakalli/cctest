class Robot
  # directions
  NORTH = 'NORTH'
  SOUTH = 'SOUTH'
  EAST = 'EAST'
  WEST = 'WEST'

  # commands
  PLACE = 'PLACE'
  MOVE = 'MOVE'
  LEFT = 'LEFT'
  RIGHT = 'RIGHT'

  REPORT = 'REPORT'

  DIRECTION_MAPPER = { "NORTH_LEFT": WEST, "NORTH_RIGHT": EAST,
                       "SOUTH_LEFT": EAST, "SOUTH_RIGHT": WEST,
                       "EAST_LEFT": NORTH, "EAST_RIGHT": SOUTH,
                       "WEST_LEFT": SOUTH, "WEST_RIGHT": NORTH }.freeze
  attr_accessor :x, :y, :orientation, :steps

  def initialize
    @x = 0
    @y = 0
    @orientation = NORTH
    @steps = []
  end

  def find_place_command(commands)
    commands.each_with_index do |c, i|
      return i if c[0..4] == PLACE
    end
    -1
  end

  def execute(commands)
    start_index = find_place_command(commands)
    return if start_index == -1

    (start_index..commands.length).each do |_i|
      commands.each do |command|
        if command[0..4] == PLACE
          place_robot(command)
        elsif command == MOVE
          move_robot
        elsif [LEFT, RIGHT].include?(command)
          rotate_robot(command)
        elsif command == REPORT
          @steps.push([@x, @y, @orientation])
        end
      end
    end
  end

  def place_robot(command)
    cords = command.split(' ')[1].split(',')
    @x = cords[0].to_i
    @y = cords[1].to_i
    @orientation = cords[2]
  end

  def rotate_robot(command)
    @orientation = DIRECTION_MAPPER["#{@orientation}_#{command}".to_sym]
  end

  def move_robot
    return unless valid_move?

    if @orientation == NORTH
      @y += 1
    elsif @orientation == SOUTH
      @y -= 1 if @y > 1
    elsif @orientation == EAST
      @x += 1
    elsif @orientation == WEST
      @x -= 1 if @x > 1
    end
  end

  def valid_move?
    (0...5).include?(@x) && (0...5).include?(@y)
  end
end
